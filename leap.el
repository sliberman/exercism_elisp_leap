;;; leap.el --- Leap exercise (exercism)

;;; Commentary:

;;; Code:
(defun leap-year-p
    (year)
  "Determine if year is a leap year"
  (let
      ((divisible-by-4 (is-divisible-by year 4))
       (divisible-by-100 (is-divisible-by year 100))
       (divisible-by-400 (is-divisible-by year 400)))
    (and divisible-by-4
	 (or (not divisible-by-100)
	     (and divisible-by-100 divisible-by-400)))))

(defun is-divisible-by
    (num denominator)
  "Return t if num is divisible by the denominator"
  (= (mod num denominator) 0))

(provide 'leap)
;;; leap.el ends here
